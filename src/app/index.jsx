// Core
import React from 'react';
import { ApolloProvider } from '@apollo/react-hooks';
import { Route, Switch} from 'react-router-dom';

// Components
import { Pet } from './bus/pet';
import { PetReturned } from './bus/pet/petReturned';

// Other 
import {client} from "./init/client";
import { Customer } from './bus/customer';
import { CheckOutPet } from './bus/pet/checkOutPet';
import { CheckInPet } from './bus/pet/checkInPet';


export const App = () => {
    return (
        <ApolloProvider client={client}>
            <Switch>
                <Route path='/' exact render={() => <MainCOntent /> } /> 
                <Route path='/pet' render={() => <CheckPet /> } /> 
            </Switch>
            
        </ApolloProvider>
    )
}

const MainCOntent = () => {
    return (
        <div className="mainBox">
                <div className="mainBox-item">
                    <Customer />
                </div>
                <div className="mainBox-item">
                    <Pet />
                </div>
            </div>
    )
}

const CheckPet = () => {
    return (
        <>
            <div className="mainBox">
                <div className="mainBox-item">
                    <CheckOutPet />
                </div>
                <div className="mainBox-item">
                    <CheckInPet />
                </div>
            </div>
            <div className="mainBox">
                <div className="mainBox-item">
                    <PetReturned />
                </div>
            </div>
        </>
    )
}