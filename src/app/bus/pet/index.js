// Cors
import React from 'react';
import { CheckInPet } from './checkInPet';

// Components
import { Counter } from './counter';
import { SpecialList } from './hooks/specialList';
import { List } from './list';
import { Profile } from './profile';

export const Pet = () => {

    return (
        <> 
            <h1> Pet </h1>
            <CheckInPet />
            <br />
            <br />
            <Profile />
            <Counter />  
            <SpecialList />
            <List />
            
        </>
    )
}