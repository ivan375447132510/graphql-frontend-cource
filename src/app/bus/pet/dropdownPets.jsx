import { useQueryAllPets } from "./hooks/useQueryAllPets"

export const DropdownPets = (props) => {
    const {loading, pets, error} = useQueryAllPets()

    const {changeSelect} = props;

    const loadingJSX = loading && (
        <p> ...идёт загрузка </p>
    )
    const errorJSX = error && (
        <p> error: {error.message} </p>
    )
    
    

    const handleChange = (e) => {
        changeSelect(e.target.value);
    }
    const petsJSX = pets && (
        <select onChange={handleChange}>
            <option>select pet </option>
            { pets.map((pet, idx) => (
                <option key={idx} value={pet.id}>{pet.name}</option>
                
            )) }
        </select>
    )
    return (
        <>
            {loadingJSX}
            {errorJSX}
            {petsJSX}
        </>
    )
}