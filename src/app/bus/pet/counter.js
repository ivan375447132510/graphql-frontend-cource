// Cors
import React from 'react';

// Hooks
import {useQueryAvaliablePets} from "./hooks/useQueryAvaliablePets";

export const Counter = () => {

    const {loading, error, data, refresh, networkStatus} = useQueryAvaliablePets();

    /* 
        refresh - запросить обновление даных
        networkStatus - возврачает статусы, пример: что сейчас происходит loading/refresh
    */

    const loadingJSX = loading && (
        <div> loading... </div>
    )

    const errorJSX = error && (
        <p> {error.message} </p>
    )

    const content = data && (
        <div>AvaliablePets: {data?.availablePets}</div>
    )

    return (
        <>
        {errorJSX}
        {loadingJSX}
        {content}
        </>
    )
}