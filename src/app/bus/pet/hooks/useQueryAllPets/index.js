// Core
import {useQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const QueryAllPets = loader("./gql/queryAllPet.gql");

export const useQueryAllPets = () => {
    const {loading, data, error} = useQuery(QueryAllPets);

    return { loading, pets: data?.allPets, error}
}