import {useSubscription} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const subscriptionPetReturned = loader("./gql/subscriptionPetReturned.gql");

export const usePetReturned = () => {
    const {loading, data, error} = useSubscription(subscriptionPetReturned);

    return { loading, error, pet: data?.petReturned?.pet}
}