import {useState} from "react";
import {useMutation} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const mutationCheckOut = loader("./gql/mutationCheckOut.gql");

export const useCheckOut = () => {
    const [_checkOut, {loading, data, error}] = useMutation(mutationCheckOut);

    const [errors, setErrors] = useState(false);

    const checkOut = (id) => {
        (
            async () => {
                try {
                    await _checkOut({
                        variables: {
                            id
                        }
                    })
                } catch (error) {
                    setErrors(error.message);
                }
                
            }
        )()
    }

    const pet = data?.checkOut.pet

    return {
        checkOut,
        loading,
        error,
        errors,
        pet
    }
    
}