import {useState} from "react";
import {useMutation} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const mutationCheckIn = loader("./gql/mutationCheckIn.gql");

export const useCheckIn = () => {
    const [_checkIn, {loading, data, error}] = useMutation(mutationCheckIn);
    const [errors, setErrors] = useState(false);
    const checkIn = (id) => {
        (
            async () => {
                try {
                    await _checkIn({
                        variables: {
                            id
                        }
                    })
                } catch (error) {
                    setErrors(error.message);
                }
                
            }
        )()
    }

    const pet = data?.checkIn.pet

    return {checkIn, pet, loading, errors, error};

}