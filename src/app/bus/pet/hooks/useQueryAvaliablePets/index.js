// core
import { useQuery } from "@apollo/react-hooks"; 
import {loader} from "graphql.macro";

// Queries
const QueryAvaliablePets = loader("./gql/queryAvaliablePets.gql");

export const useQueryAvaliablePets = () => {
    return useQuery(QueryAvaliablePets, {
        // variables: { type },
        // pollInterval: 5000,
        // skip: !type, // пропустить запрос если поле type === false
    });
}