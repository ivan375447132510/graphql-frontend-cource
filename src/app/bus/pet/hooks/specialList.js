//  Core
import React from 'react';
import { useQueryAllAvailablePets } from './useQueryAllAvailablePets';

// Hooks


export const SpecialList = () => {

const {getAllAvailablePets, loading, error, pets} = useQueryAllAvailablePets()

    const loadingJSX = loading && (
        <div> Loading... </div>
    )

    const errorJSX = error && (
        <div> {error.message} </div>
    )

    const petsJSX = pets?.map(({name}, index) => (
        <div>{name}</div>
    ));

    return (
        <>
            {loadingJSX}
            {errorJSX}
            {petsJSX}
            <div> 
                <button onClick={getAllAvailablePets}> get pets </button> 
            </div>
        </>
    )
}