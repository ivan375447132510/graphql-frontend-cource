// Core
import {useLazyQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

// Querys
const QueryPetById = loader("./gql/queryProfile.gql");

export const useQueryProfile = () => {
    const [getProfile, {loading, data, error}] = useLazyQuery(QueryPetById);

    return { getProfile, loading, profile: data?.petById, error }
}