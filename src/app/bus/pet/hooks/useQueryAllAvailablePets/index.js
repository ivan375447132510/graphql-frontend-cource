import {useLazyQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const QueryAllAvailablePets = loader("./gql/allAvailablePets.gql");

export const useQueryAllAvailablePets = () => {
    const [getAllAvailablePets, {loading, data, error}] = useLazyQuery(QueryAllAvailablePets);

    return {getAllAvailablePets, loading, error, pets: data?.allAvailablePets}
}