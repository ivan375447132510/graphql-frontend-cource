// Hooks
import { DropdownPets } from "./dropdownPets";
import { useCheckIn } from "./hooks/useCheckIn"

export const CheckInPet = () => {

    const {checkIn, pet, loading, errors, error} = useCheckIn();

    if(error){
        console.log("error", errors)
    }
    
    const handleChange = (id) => {
        checkIn(id);
    }

    const loadingJSX = loading && (
        <p> ...идёт загрузка </p>
    )

    const petJSX = pet && (
        <ul>
            <li> id: {pet.id}</li>
            <li>name:  {pet.name}</li>
            <li>weight: {pet.weight}</li>
        </ul>
    )
    const errorsJSX = errors && (
        <p> errors: {errors.message} </p>
    )
    const errorJSX = error && (
        <p> errors: {error} </p>
    )

    return (
        <>
            <h1> checkIn pet </h1>
            <DropdownPets changeSelect={handleChange} />
            {loadingJSX}
            {errorJSX && errorsJSX}
            {petJSX}
        </>
    )
}