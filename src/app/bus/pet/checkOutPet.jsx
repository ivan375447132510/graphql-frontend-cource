import { DropdownPets } from "./dropdownPets"
import { useCheckOut } from "./hooks/useCheckOut"

export const CheckOutPet = () => {

    const { checkOut, loading, error, errors, pet} = useCheckOut();

    const handleChange = (id) => {
        checkOut(id);
    }

    const loadingJSX = loading && (
        <p> ...идёт загрузка </p>
    )
    const errorJSX = error && (
        <p> error: {error.message} </p>
    )
    const errorsJSX = errors && (
        <p> error: {errors} </p>
    )

    const petJSX = pet && (
        <ul>
            <li> name: {pet.name} </li>
            <li> weight:  {pet.weight}</li>
            <li> <img width="200"  height="200" src={pet.photo.thumb} alt={pet.__typename} /> </li>
        </ul>
    )

    return (
        <>
            <h1> CheckOutPet </h1>
            <DropdownPets changeSelect={handleChange} />
            <br />
            {loadingJSX}
            {errorJSX && errorsJSX}
            {petJSX}
        </>

    )
}