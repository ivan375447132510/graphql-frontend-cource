// Core
import React from 'react';

// Hooks
import { usePetReturned } from './hooks/usePetReturned';

export const PetReturned = () => {

    const {loading, pet, error} = usePetReturned();

    const loadingJSX = loading && (
        <p> empty data </p>
    )
    const petJSX = pet && (
        <ul>
            <li> id: {pet.id}</li>
            <li>name:  {pet.name}</li>
            <li>weight: {pet.weight}</li>
            <li> type: {pet.__typename}</li>
        </ul>
    )
    const errorJSX = error && (
        <p> errors: {error} </p>
    )

    return (
        <>
            <h1> pet returned </h1>
            {loadingJSX}
            {errorJSX}
            {petJSX}
        </>
    )
}