import React from 'react';

// Hooks
import {useQueryProfile} from "./hooks/useQueryProfile";

export const Profile = () => {

    const { getProfile, loading, profile, error } = useQueryProfile()
    
    const loadProfile = () => {
        getProfile({
            variables: {
                id: "C-1"
            }
        });
    }

    const loadingJSX = loading && (
        <div> loading... </div>
    )
    const errorJSX = error && (
        <div> error: {error.message} </div>
    )

    return (
        <>
        <button onClick={loadProfile}> get profile </button>
        {loadingJSX}
        {errorJSX}
        <div> {profile?.name} </div>
        </>
    )
}