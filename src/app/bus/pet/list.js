// Cors
import React from 'react';
//  Hooks
import { useQueryAllPets } from './hooks/useQueryAllPets';

export const List = () => {

    const {loading, pets, error} = useQueryAllPets();

    const loadingJSX = loading && (
        <div> loading... </div>
    )
    const errorJSX = error && (
        <div> {error.message} </div>
    )

    const petsJSX = pets?.map((elem, index) => (
            <li key={index} style={{borderBottom: "1px solid #000", margin: "5px", padding: "5px"}}>
                <div>name: {elem.name}</div>
                <div>weight: {elem.weight}</div>
            </li>
        ))
    

    return <>
        {loadingJSX}
        {errorJSX}
        <ul>
            {petsJSX}
        </ul>
    </>
}