// Core
import React from 'react';

// Hooks
import {useCustomerCreator} from "./hooks/useCustomerCreator";

export const Create = () => {

        

    const {handleChange, onSave, createAccount, loading, error} = useCustomerCreator();

    const errorJSX = error && (
        <div> {error.message} </div>
    )
    const loadingJSX = loading && (
        <p> ...Идёт регистрация </p>
    )

    const contentJSX = createAccount && (
                <>
                    <p> {createAccount?.name}: Вы успешно зарегестрировались. Ваш логин для входа {createAccount?.username} </p>
                    <p> Дата регистрации {createAccount?.dateCreated} </p>
                </>
    )
    return (
        <>
        
            <div> Registration</div>
            {!loading && (
                <>
                    <input type="text" name="name" placeholder="Name" onChange={handleChange} />
                    <input type="text" name="username" placeholder="Username" onChange={handleChange} />
                    <input type="password" name="password" placeholder="Password" onChange={handleChange} />
                    <button onClick={onSave}> save </button>
                </>
            )}
            {loadingJSX}
            {contentJSX}
            {errorJSX}
        </>
    )

}