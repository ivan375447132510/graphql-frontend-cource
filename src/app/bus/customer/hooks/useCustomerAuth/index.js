import { useState, useEffect } from "react";
import {useMutation} from "@apollo/react-hooks";
import {loader} from "graphql.macro";
import useForm from '../../../../init/hooks/useForm';

const mutationLogin = loader("./gql/mutationLogin.gql");

export const useCustomerAuth = () => {
    const [_logIn, {loading, data, error}] = useMutation(mutationLogin);
    const { handleChange, values } = useForm({
        username: '',
        password: '',
    })
    const [isAuth, setIsAuth] = useState(false);

    useEffect(() => {
        if(data?.logIn){
            localStorage.setItem("token", data.logIn.token);
        }
    
        if(localStorage.getItem("token")){
            setIsAuth(true);
        }
    }, [data])

    const logIn = () => {
        _logIn({
            variables: values
        })
    }


    return {
        handleChange,
        logIn,
        isAuth: isAuth,
        loading,
        error
    }

}