import React from 'react'

import {useMutation} from "@apollo/react-hooks";
import {loader} from "graphql.macro";
import useForm from '../../../../init/hooks/useForm';

const mutationCreateAccount = loader("./gql/mutationCreateAccount.gql");

export const useCustomerCreator = () => {

    const [addUser, {loading, data, error}] = useMutation(mutationCreateAccount);
    const { handleChange, values } = useForm({
        name: '',
        username: '',
        password: '',
    })

    const onSave = () => {
        console.log("values ", values);
        // const { account } = values;
        addUser({
            variables: {
                account: values
            }
        })
    }

    return {
        values,
        handleChange,
        onSave,
        createAccount: data?.createAccount,
        loading,
        error
    }

}