import {useQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const QueryAllCustomers = loader("./gql/allCustomers.gql");

export const useQueryAllCustomers = () => {
    const {loading, data, error} = useQuery(QueryAllCustomers);
    
    return {loading, customers: data?.allCustomers, error}
}