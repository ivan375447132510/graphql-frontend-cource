// Core
import React from 'react';
import { Login } from './login';
import { Create } from './customer';

// Components
import {List} from "./list";

export const Customer = () => {
    return (
        <>
            <h2> Customer </h2>
            <Login />
            <Create/>
            <List />
        </>
    )
}