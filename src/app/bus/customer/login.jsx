// Core
import React from 'react';

// Hooks
import { useCustomerAuth } from './hooks/useCustomerAuth';

export const Login = () => {

    const {handleChange, logIn, isAuth, loading, error} = useCustomerAuth();

    

    const errorJSX = error && (
        <div> {error.message} </div>
    )
    const loadingJSX = loading && (
        <p> ...Идёт регистрация </p>
    )

    const contentJSX = isAuth && (
                    <p> добро пожаловать </p>
    )
    return (
        <>
        
            <div> Authorization</div>
            {!loading && (
                <>
                    <input type="text" name="username" placeholder="Username" onChange={handleChange} />
                    <input type="password" name="password" placeholder="Password" onChange={handleChange} />
                    <button onClick={logIn}> login </button>
                </>
            )}
            {loadingJSX}
            {contentJSX}
            {errorJSX}
        </>
    )

}




