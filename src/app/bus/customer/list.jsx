// Core
import React from 'react';

// Hooks
import {useQueryAllCustomers} from "./hooks/useQueryAllCustomers";

export const List = () => {

    const {loading, customers, error} = useQueryAllCustomers();

    const loadingJSX = loading && (
        <div> Идет загрузка... </div>
    )

    const errorJSX = error && (
        <div> error: {error.message}</div>
    )
    
    const customersJSX = customers && (
        <table>
            <tr>
                <td>Никнейм</td>
                <td>Имя</td>
                <td>Дата создания</td>
            </tr>
            
                {customers.map((elem, index) => (
                    <tr key={index}>
                        <td>{elem.username}</td>
                        <td>{elem.name}</td>
                        <td>{elem.dateCreated}</td>
                    </tr>
                        )
                )}
                
        </table>
    )
    return (
        <>
            {loadingJSX}
            {errorJSX}
            {customersJSX}
        </>
    )
}