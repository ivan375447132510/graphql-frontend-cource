import React, {useState} from 'react'

const useForm = (initialValues) => {
    const [values, setValue] = useState(initialValues);


    const handleChange = (event) => {
        event.persist();
        setValue((prevValues) => ({
                ...prevValues, 
                [event.target.name]: event.target.value
        }))
    }

    return { handleChange, values }

}




export default useForm